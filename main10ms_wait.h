#ifndef _MAIN10MS_WAIT_H_ //多重インクルードガード
#define _MAIN10MS_WAIT_H_
#include "arduino.h" //Arduinoのクラスを使うよってこと

class main10ms_wait{
  public: //クラスの外から使うモノはpublicに
    main10ms_wait(void); //インスタンス
    void ini(unsigned short ms=10); //初期化関数
    unsigned long wait(void); //10ms待ち関数
  private: //クラスの外から使わないモノはprivateに
    unsigned short usFini;
    unsigned long ulDnowtim;
    unsigned long ulDoldtim;
    unsigned long ulDmaintim;
    unsigned long ulWaitems;
};

#endif
