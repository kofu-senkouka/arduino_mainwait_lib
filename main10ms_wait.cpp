#include "main10ms_wait.h"

//インスタンスの中身
main10ms_wait::main10ms_wait(void){
    ini(); //インスタンス時に初期化する
}

void main10ms_wait::ini(unsigned short ms){
    ulDnowtim = 0; //現在時刻の変数を初期化
    ulDoldtim = 0; //前回時刻の変数を初期化
    usFini = 0; //初期化後の初回フラグクリア
    ulWaitems = ms*1000;
}

unsigned long main10ms_wait::wait(void){
    if( usFini == 1 ){
        //前回の時刻-現在の時刻が10ms以内の場合は待つ
        do{
            ulDnowtim = micros(); //現在時刻に起動後の経過時間(us)を格納
            if( ulDoldtim <= ulDnowtim ){
                //タイマーが１周していない（正常）な時
                ulDmaintim = ulDnowtim - ulDoldtim; // 現在時刻 - 前回の時刻
            }
            else{
                //タイマーが１周している（オーバーフロー）時
                ulDmaintim = (0xFFFFFFFF - ulDoldtim) + ulDnowtim; //(最大値－前回の時刻)＋現在時刻
            }
        }while( ulDmaintim < ulWaitems);
    }
    else{
        //初期化後の初回なので、処理なし
    }
    ulDoldtim = micros(); //前回の時刻として現在時刻を格納
    usFini = 1; //初期化後の初回フラグをセット
    return (ulDmaintim);

}
